var nDebugLevel = 0;
var bFullSpeed = false;
var bIsReset = false;
var sTape = "";
var nTapeOffset = 0;		/* posisi logic pada TM tape pada karakter pertama pada sTape */
var nHeadPosition = 0;		/* the position of the TM's head on its tape. Initially zero; may be negative if TM moves to left */
var sState = "0";
var nSteps = 0;
var nVariant = 0; /* standard infinite tape */
var hRunTimer = null;
var aProgram = new Object();
var nMaxUndo = 10;
var aUndoList = [];
var nTextareaLines = -1;
var oTextarea;
var bIsDirty = true;	/* jika true, source harus direcompiled sebelum running machine */
var oNextLineMarker = $("<div class='NextLineMarker'>Next<div class='NextLineMarkerEnd'></div></div>");
var oPrevLineMarker = $("<div class='PrevLineMarker'>Prev<div class='PrevLineMarkerEnd'></div></div>");
var oPrevInstruction = null;

var sPreviousStatusMsg = "";

//menjalankan step by step
function Step()
{
	if( bIsDirty) Compile();

	bIsReset = false;
	if( sState.substring(0,4).toLowerCase() == "halt" ) {
		EnableControls( false, false, false, true, true, true, true );
		return( false );
	}

	var sNewState, sNewSymbol, nAction, nLineNumber;

	/* mendapatkan current symbol */
	var sHeadSymbol = GetTapeSymbol( nHeadPosition );

	/* menemukan instruksi TM yang sesuai */
	var aInstructions = GetNextInstructions( sState, sHeadSymbol );
	var oInstruction;
	if( aInstructions.length == 0 ) {
    // tidak ada instruksi yang sesuai. Error dihandle dibawah ini
    oInstruction = null;
	} else {
    // Deterministic TM
    oInstruction = aInstructions[0];
	}

	if( oInstruction != null ) {
		sNewState = (oInstruction.newState == "*" ? sState : oInstruction.newState);
		sNewSymbol = (oInstruction.newSymbol == "*" ? sHeadSymbol : oInstruction.newSymbol);
		nAction = (oInstruction.action.toLowerCase() == "r" ? 1 : (oInstruction.action.toLowerCase() == "l" ? -1 : 0));
    if( nVariant == 1 && nHeadPosition == 0 && nAction == -1 ) {
      nAction = 0;  /* Tidak dapat move kekiri ketika sudah ada pada sel left-most tape */
    }
		nLineNumber = oInstruction.sourceLineNumber;
	} else {
		sNewState = "halt";
		sNewSymbol = sHeadSymbol;
		nAction = 0;
		nLineNumber = -1;
	}

	/* Menyimpan informasi data undo */
  if( nMaxUndo > 0 ) {
    if( aUndoList.length >= nMaxUndo ) aUndoList.shift();  /* Membatalkan inputan undo yang paling Discard oldest undo entry */
    aUndoList.push({state: sState, position: nHeadPosition, symbol: sHeadSymbol});
  }

	/* Update machine tape & state */
	SetTapeSymbol( nHeadPosition, sNewSymbol );
	sState = sNewState;
	nHeadPosition += nAction;

	nSteps++;

	oPrevInstruction = oInstruction;
	UpdateInterface();

	if( sNewState.substring(0,4).toLowerCase() == "halt" ) {
		if( oInstruction != null ) {
		}
		EnableControls( false, false, false, true, true, true, true );
		return( false );
	} else {
		if( oInstruction.breakpoint ) {
			EnableControls( true, true, false, true, true, true, true );
			return( false );
		} else {
			return( true );
		}
	}
}

/* Undo(): undo single step */
function Undo()
{
  var oUndoData = aUndoList.pop();
  if( oUndoData ) {
    nSteps--;
    sState = oUndoData.state;
    nHeadPosition = oUndoData.position;
    SetTapeSymbol( nHeadPosition, oUndoData.symbol );
    oPrevInstruction = null;
    EnableControls( true, true, false, true, true, true, true );
    UpdateInterface();
  }
}


/* Menjalankan TM sampai halts atau sampai diinterupsi oleh user */
function Run()
{
  var bContinue = true;
  if( bFullSpeed ) {
    /* menjalankan 25 steps dalam satu waktu pada mode cepat */
    for( var i = 0; bContinue && i < 25; i++ ) {
      bContinue = Step();
    }
    if( bContinue ) hRunTimer = window.setTimeout( Run, 10 );
    else UpdateInterface();   /* sewaktu-waktu tidak semuanya ditampilkan, karena mode cepat */
  } else {
    /* menjalankan program dalam 50ms setiap step pada mode normal*/
    if( Step() ) {
      hRunTimer = window.setTimeout( Run, 50 );
    }
  }
}

function RunStep()
{
	if( !Step() ) {
		StopTimer();
	}
}

/* menonaktifkan run timer. */
function StopTimer()
{
	if( hRunTimer != null ) {
		window.clearInterval( hRunTimer );
		hRunTimer = null;
	}
}

/* inisiasi ulang TM */
function Reset()
{
	var sInitialTape = $("#InitialInput")[0].value;

	nHeadPosition = sInitialTape.indexOf( "*" );
	if( nHeadPosition == -1 ) nHeadPosition = 0;

	sInitialTape = sInitialTape.replace( /\*/g, "" ).replace( / /g, "_" );
	if( sInitialTape == "" ) sInitialTape = " ";
	sTape = sInitialTape;
	nTapeOffset = 0;

	var sInitialState = $("#InitialState")[0].value;
	sInitialState = $.trim( sInitialState ).split(/\s+/)[0];
	if( !sInitialState || sInitialState == "" ) sInitialState = "0";
	sState = sInitialState;

  var dropdown = $("#MachineVariant")[0];
  nVariant = Number(dropdown.options[dropdown.selectedIndex].value);
  SetupVariantCSS();

	nSteps = 0;
	bIsReset = true;

	Compile();
	oPrevInstruction = null;

	aUndoList = [];

	EnableControls( true, true, false, true, true, true, false );
	UpdateInterface();
}

function createTuringInstructionFromTuple( tuple, line )
{
	return {
		newSymbol: tuple.newSymbol,
		action: tuple.action,
		newState: tuple.newState,
		sourceLineNumber: line,
		breakpoint: tuple.breakpoint
	};
}

function isArray( possiblyArr )
{
	Object.prototype.toString.call(possiblyArr) === "[object Array]";
}

/* menjalankan state yang diinputkan */
function Compile()
{
	var sSource = oTextarea.value;
	debug( 2, "Compile()" );

	SetSyntaxMessage( null );
	ClearErrorLines();

	aProgram = new Object;

	sSource = sSource.replace( /\r/g, "" );

	var aLines = sSource.split("\n");
	for( var i = 0; i < aLines.length; i++ )
	{
		var oTuple = ParseLine( aLines[i], i );
		if( oTuple.isValid ) {
			if( aProgram[oTuple.currentState] == null ) aProgram[oTuple.currentState] = new Object;
			if( aProgram[oTuple.currentState][oTuple.currentSymbol] == null ) {
        aProgram[oTuple.currentState][oTuple.currentSymbol] = [];
			}
			if( aProgram[oTuple.currentState][oTuple.currentSymbol].length > 0 && nVariant != 2 ) {
        // Multiple conflicting instructions found.
        SetErrorLine( i );
        SetErrorLine( aProgram[oTuple.currentState][oTuple.currentSymbol][0].sourceLineNumber );
        aProgram[oTuple.currentState][oTuple.currentSymbol][0] = createTuringInstructionFromTuple( oTuple, i );
			} else {
        aProgram[oTuple.currentState][oTuple.currentSymbol].push( createTuringInstructionFromTuple( oTuple, i ) );
      }
		}
	}


	/* line telah berubah. Line sebelumnya tidak berarti lagi, kalkulasi ualng line berikutnya. */
	oPrevInstruction = null;

	bIsDirty = false;

	UpdateInterface();
}

function ParseLine( sLine, nLineNum )
{
	sLine = sLine.split( ";", 1 )[0];
	var aTokens = sLine.split(/\s+/);
	aTokens = aTokens.filter( function (arg) { return( arg != "" ) ;} );

	var oTuple = new Object;

	if( aTokens.length == 0 )
	{
		oTuple.isValid = false;
		return( oTuple );
	}

	oTuple.currentState = aTokens[0];

	if( aTokens.length < 2 ) {
		oTuple.isValid = false;
		return( oTuple );
	}
	if( aTokens[1].length > 1 ) {
		oTuple.isValid = false;
		return( oTuple );
	}
	oTuple.currentSymbol = aTokens[1];

	if( aTokens.length < 3 ) {
		oTuple.isValid = false;
		return( oTuple );
	}
	if( aTokens[2].length > 1 ) {
		oTuple.isValid = false;
		return( oTuple );
	}
	oTuple.newSymbol = aTokens[2];

	if( aTokens.length < 4 ) {
		oTuple.isValid = false;
		return( oTuple );
	}
	if( ["l","r","*"].indexOf( aTokens[3].toLowerCase() ) < 0 ) {
		oTuple.isValid = false;
		return( oTuple );
	}
	oTuple.action = aTokens[3].toLowerCase();

	if( aTokens.length < 5 ) {
		oTuple.isValid = false;
		return( oTuple );
	}
	oTuple.newState = aTokens[4];

	if( aTokens.length > 6 ) {
		oTuple.isValid = false;
		return( oTuple );
	}
	if( aTokens.length == 6 ) {
		if( aTokens[5] == "!" ) {
			oTuple.breakpoint = true;
		} else {
			oTuple.isValid = false;
			return( oTuple );
		}
	} else {
		oTuple.breakpoint = false;
	}

	oTuple.isValid = true;
	return( oTuple );
}

function GetNextInstructions( sState, sHeadSymbol )
{
  var result = null;
	if( aProgram[sState] != null && aProgram[sState][sHeadSymbol] != null ) {
		/* Use instructions specifically corresponding to current state & symbol, if any */
		return( aProgram[sState][sHeadSymbol] );
	} else if( aProgram[sState] != null && aProgram[sState]["*"] != null ) {
		/* Next use rules for the current state and default symbol, if any */
		return( aProgram[sState]["*"] );
	} else if( aProgram["*"] != null && aProgram["*"][sHeadSymbol] != null ) {
		/* Next use rules for default state and current symbol, if any */
		return( aProgram["*"][sHeadSymbol] );
	} else if( aProgram["*"] != null && aProgram["*"]["*"] != null ) {
		/* Finally use rules for default state and default symbol */
		return( aProgram["*"]["*"] );
	} else {
    return( [] );
  }
}

/* mengembalikan simbol pada sel n dari TM tape */
function GetTapeSymbol( n )
{
	if( n < nTapeOffset || n >= sTape.length + nTapeOffset ) {
		return( "_" );
	} else {
		var c = sTape.charAt( n - nTapeOffset );
		if( c == " " ) { c = "_";  }
		return( c );
	}
}


/* menulis simbol c ke sel n dari TM tape */
function SetTapeSymbol( n, c )
{
	if( c == " " ) { c = "_";
}
	if( n < nTapeOffset ) {
		sTape = c + repeat( "_", nTapeOffset - n - 1 ) + sTape;
		nTapeOffset = n;
	} else if( n > nTapeOffset + sTape.length ) {
		sTape = sTape + repeat( "_", nTapeOffset + sTape.length - n - 1 ) + c;
	} else {
		sTape = sTape.substr( 0, n - nTapeOffset ) + c + sTape.substr( n - nTapeOffset + 1 );
	}
}

/* Menyimpan current machine dan state sebagai objek yang cocok untuk disimpan sebagai JSON */
function SaveMachineSnapshot()
{
	return( {
		"program": oTextarea.value,
		"state": sState,
		"tape": sTape,
		"tapeoffset": nTapeOffset,
		"headposition": nHeadPosition,
		"steps": nSteps,
		"initialtape": $("#InitialInput")[0].value,
		"initialstate": $("#InitialState")[0].value,
		"fullspeed": bFullSpeed,
		"variant": nVariant,
		"version": 1
	});
}

/* Memuat machine dan state dari sebuah objek yang dibuat dari SaveMachineSnapshot */
function LoadMachineSnapshot( oObj )
{
	if( oObj.version && oObj.version != 1 );
	if( oObj.program ) oTextarea.value = oObj.program;
	if( oObj.state ) sState = oObj.state;
	if( oObj.tape ) sTape = oObj.tape;
	if( oObj.tapeoffset ) nTapeOffset = oObj.tapeoffset;
	if( oObj.headposition ) nHeadPosition = oObj.headposition;
	if( oObj.steps ) nSteps = oObj.steps;
	if( oObj.initialtape ) $("#InitialInput")[0].value = oObj.initialtape;
	if( oObj.initialstate ) {
		$("#InitialState")[0].value = oObj.initialstate;
	} else {
		$("#InitialState")[0].value = "";
	}
	if( oObj.fullspeed ) {
		$("#SpeedCheckbox")[0].checked = oObj.fullspeed;
		bFullSpeed = oObj.fullspeed;
	}
	if( oObj.variant ) {
	  nVariant = oObj.variant;
	} else {
    nVariant = 0;
	}
	TextareaChanged();
	Compile();
	UpdateInterface();
}

/* SetSyntaxMessage(): display a syntax error message in the textarea */
function SetSyntaxMessage( msg )
{
	$("#SyntaxMsg").html( (msg?msg:"&nbsp;") )
}

function RenderTape()
{
	/*
	  sFirstPart is simbol dikiri head
	  sHeadSymbol is simbol dibawah head
	  sSecondPart is simbol dikanan head
	*/
	var nTranslatedHeadPosition = nHeadPosition - nTapeOffset;  /* posisi dari head relative ke sTape */
	var sFirstPart, sHeadSymbol, sSecondPart;

	if( nTranslatedHeadPosition > 0 ) {
		sFirstPart = sTape.substr( 0, nTranslatedHeadPosition );
	} else {
		sFirstPart = "";
	}
	if( nTranslatedHeadPosition > sTape.length ) {  /* Need to append blanks to sFirstPart.  Shouldn't happen but just in case. */
		sFirstPart += repeat( " ", nTranslatedHeadPosition - sTape.length );
	}
	sFirstPart = sFirstPart.replace( /_/g, " " );

	if( nTranslatedHeadPosition >= 0 && nTranslatedHeadPosition < sTape.length ) {
		sHeadSymbol = sTape.charAt( nTranslatedHeadPosition );
	} else {
		sHeadSymbol = " ";	/* Shouldn't happen but just in case */
	}
	sHeadSymbol = sHeadSymbol.replace( /_/g, " " );

	if( nTranslatedHeadPosition >= 0 && nTranslatedHeadPosition < sTape.length - 1 ) {
		sSecondPart = sTape.substr( nTranslatedHeadPosition + 1 );
	} else if( nTranslatedHeadPosition < 0 ) {  /* Need to prepend blanks to sSecondPart. Shouldn't happen but just in case. */
		sSecondPart = repeat( " ", -nTranslatedHeadPosition - 1 ) + sTape;
	} else {  /* nTranslatedHeadPosition > sTape.length */
		sSecondPart = "";
	}
	sSecondPart = sSecondPart.replace( /_/g, " " );

	/* Menampilkan bagian-bagian dari tape */
	$("#LeftTape").text( sFirstPart );
	$("#ActiveTape").text( sHeadSymbol );
	$("#RightTape").text( sSecondPart );
	/* Scroll tampilan tape display untuk memastikan bahawa head itu visible */
	if( $("#ActiveTapeArea").position().left < 0 ) {
		$("#MachineTape").scrollLeft( $("#MachineTape").scrollLeft() + $("#ActiveTapeArea").position().left - 10 );
	} else if( $("#ActiveTapeArea").position().left + $("#ActiveTapeArea").width() > $("#MachineTape").width() ) {
		$("#MachineTape").scrollLeft( $("#MachineTape").scrollLeft() + ($("#ActiveTapeArea").position().left - $("#MachineTape").width()) + 10 );
	}
}

function RenderState()
{
	$("#MachineState").html( sState );
}

function RenderSteps()
{
	$("#MachineSteps").html( nSteps );
}

function RenderLineMarkers()
{
  var oNextList = $.map(GetNextInstructions( sState, GetTapeSymbol( nHeadPosition ) ), function(x){return(x.sourceLineNumber);} );
	SetActiveLines( oNextList, (oPrevInstruction?oPrevInstruction.sourceLineNumber:-1) );
}

/* UpdateInterface(): refresh  tape, state dan steps yang ditampilkan pada halaman */
function UpdateInterface()
{
	RenderTape();
	RenderState();
	RenderSteps();
	RenderLineMarkers();
}

function ClearDebug()
{
	$("#debug").empty();
}

function EnableControls( bStep, bRun, bStop, bReset, bSpeed, bTextarea, bUndo )
{
  document.getElementById( 'StepButton' ).disabled = !bStep;
  document.getElementById( 'RunButton' ).disabled = !bRun;
  document.getElementById( 'StopButton' ).disabled = !bStop;
  document.getElementById( 'ResetButton' ).disabled = !bReset;
  document.getElementById( 'SpeedCheckbox' ).disabled = !bSpeed;
  document.getElementById( 'Source' ).disabled = !bTextarea;
  EnableUndoButton(bUndo);
  if( bSpeed ) {
    $( "#SpeedCheckboxLabel" ).removeClass( "disabled" );
  } else {
    $( "#SpeedCheckboxLabel" ).addClass( "disabled" );
  }
}

function EnableUndoButton(bUndo)
{
  document.getElementById( 'UndoButton' ).disabled = !(bUndo && aUndoList.length > 0);
}


function StepButton()
{
	Step();
	EnableUndoButton(true);
}

function RunButton()
{
	SpeedCheckbox();
	EnableControls( false, false, true, false, false, false, false );
	Run();
}

function StopButton()
{
	if( hRunTimer != null ) {
		EnableControls( true, true, false, true, true, true, true );
		StopTimer();
	}
}

function ResetButton()
{
	Reset();
	EnableControls( true, true, false, true, true, true, false );
}

function SpeedCheckbox()
{
  bFullSpeed = $( '#SpeedCheckbox' )[0].checked;
}

function SetupVariantCSS()
{
  if( nVariant == 1 ) {
    $("#LeftTape").addClass( "OneDirectionalTape" );
  } else {
    $("#LeftTape").removeClass( "OneDirectionalTape" );
  }
}

function LoadSampleProgram( zName, zFriendlyName, bInitial )
{
	var zFileName = "contoh-state/" + zName + ".txt";

	StopTimer();   /* Menghentikan mesin, jika sedang berjalan */

	$.ajax({
		url: zFileName,
		type: "GET",
		dataType: "text",
		success: function( sData, sStatus, oRequestObj ) {
			/* Load  default initial tape*/
			var oRegExp = new RegExp( ";.*\\$INITIAL_TAPE:? *(.+)$" );
			var aRegexpResult = oRegExp.exec( sData );
			if( aRegexpResult != null && aRegexpResult.length >= 2 ) {
				$("#InitialInput")[0].value = aRegexpResult[1];
				sData = sData.replace( /^.*\$INITIAL_TAPE:.*$/m, "" );
			}
			$("#InitialState")[0].value = "0";
			nVariant = 0;
			$("#MachineVariant").val(0);
			oTextarea.value = sData;
			TextareaChanged();
			Compile();

			Reset();
		},
	});

	ClearSaveMessage();
}

function TextareaChanged()
{
	var nNewLines = (oTextarea.value.match(/\n/g) ? oTextarea.value.match(/\n/g).length : 0) + 1;
	if( nNewLines != nTextareaLines ) {
		nTextareaLines = nNewLines
		UpdateTextareaDecorations();
	}

	bIsDirty = true;
	oPrevInstruction = null;
	RenderLineMarkers();
}

/* Generate line numbers setiap line pada textarea */
function UpdateTextareaDecorations()
{
	var oBackgroundDiv = $("#SourceBackground");

	oBackgroundDiv.empty();

	var sSource = oTextarea.value;
	sSource = sSource.replace( /\r/g, "" );

	var aLines = sSource.split("\n");

	for( var i = 0; i < aLines.length; i++)
	{
		oBackgroundDiv.append($("<div id='talinebg"+(i+1)+"' class='talinebg'><div class='talinenum'>"+(i+1)+"</div></div>"));
	}

	UpdateTextareaScroll();
}

function SetActiveLines( next, prev )
{
	$(".talinebgnext").removeClass('talinebgnext');
	$(".NextLineMarker").remove();
	$(".talinebgprev").removeClass('talinebgprev');
	$(".PrevLineMarker").remove();

  var shift = false;
	for( var i = 0; i < next.length; i++ )
	{
    var oMarker = $("<div class='NextLineMarker'>Next<div class='NextLineMarkerEnd'></div></div>");
    $("#talinebg"+(next[i]+1)).addClass('talinebgnext').prepend(oMarker);
    if( next[i] == prev ) {
      oMarker.addClass('shifted');
      shift = true;
    }
	}
	if( prev >= 0 )
	{
    var oMarker = $("<div class='PrevLineMarker'>Prev<div class='PrevLineMarkerEnd'></div></div>");
    if( shift ) {
      $("#talinebg"+(prev+1)).prepend(oMarker);
      oMarker.addClass('shifted');
    } else {
      $("#talinebg"+(prev+1)).addClass('talinebgprev').prepend(oMarker);
    }
	}
}

function SetErrorLine( num )
{
	$("#talinebg"+(num+1)).addClass('talinebgerror');
}

function ClearErrorLines()
{
	$(".talinebg").removeClass('talinebgerror');
}

/* Update line numbers ketika textarea di scroll */
function UpdateTextareaScroll()
{
	var oBackgroundDiv = $("#SourceBackground");

	$(oBackgroundDiv).css( {'margin-top': (-1*$(oTextarea).scrollTop()) + "px"} );
}

/* OnLoad function untuk HTML body. */
function OnLoad()
{
	if( nDebugLevel > 0 ) $(".DebugClass").toggle( true );

	oTextarea = $("#Source")[0];
	TextareaChanged();

	if( window.location.search != "" ) {
		LoadFromCloud( window.location.search.substring( 1 ) );
		window.history.replaceState( null, "", window.location.pathname );  /* menghapus query string dari URL */
	} else {
		LoadSampleProgram( 'perkalian', 'Default program', true );
	}
}

/* return a string of n copies of c */
function repeat( c, n )
{
	var sTmp = "";
	while( n-- > 0 ) sTmp += c;
	return sTmp;
}

function debug( n, str )
{
	if( n <= 0 ) {
		console.log( str );
	}
	if( nDebugLevel >= n  ) {
		$("#debug").append( document.createTextNode( str + "\n" ) );
		console.log( str );
	}
}
